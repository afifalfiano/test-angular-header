import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NgResearchLibModule } from '@research/ng-research-lib';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    NgResearchLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
